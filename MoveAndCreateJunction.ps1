# Copyright (c) 2018 Christopher Gundlach
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

<#
.SYNOPSIS
Moves a folder and creates a junction to the new location in the original location
.DESCRIPTION
This script moves a folder and creates a junction in the original location. 
This is especially useful if there are other scripts or applications which expect the original location or if you want to move an installed application without having to uninstall it.
.EXAMPLE
MoveAndCreateJunction.ps1 -Path C:\Test -Target C:\Target
#>


param (
    [string]$Path,
    [string]$Target
)

if (-not ($Path)) {
    "Kein Pfad angegeben"
    pause
    return
}

if (-not ($Target)) {
    "Kein Ziel angegeben"
    pause
    return
}

if (-not (Test-Path -Path $Path)) {
    "Ursprungsordner existiert nicht"
    pause
    return
}
if (Test-Path -Path $Target) {
    "Zielordner existiert bereits"
    pause
    return
}

Move-Item -Path $Path -Destination $Target
New-Item -Type Junction -Path $Path -Target $Target