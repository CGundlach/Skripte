# Copyright (c) 2018 Christopher Gundlach
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

<#
.SYNOPSIS
Startet ein anderes Skript ohne das Fenster des Skripts anzuzeigen
.DESCRIPTION
Die gestarteten Skripte zeigen kein Fenster an, ideal wenn sie im Hintergrund laufen sollen. 
Gibt das Prozess-Objekt der gestarteten PowerShell-Instanz zurueck.

Das Skript kann so modifiziert werden, dass es ohne Parameter funktioniert.
Da bei der Windows-Aufgabenverwaltung keine Parameter wie '-WindowsStyle Hidden' an PowerShell weitergegeben werden koennen,
    kann dieses Skript als Mittelsmann dienen, wenn ein Pfad im Skript angegeben wird.

Damit Skripte gestartet werden koennen muss Powershell als Standardprogramm fuer '.ps1'-Dateien konfiguriert sein.
.EXAMPLE
RunScriptHidden -Path D:\BeispielSkript.ps1
#>

param(
    [string]$Path
)
# Um den Start ohne Parameter zu ermöglichen, folgende Zeile auskommentieren und Pfad angeben
# $Path = "Pfad eingeben"

return Start-Process -FilePath $Path -WindowStyle Hidden -PassThru