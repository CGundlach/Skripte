$monitoredDirectory = 'D:\Test\Test-Ordner'                                # Hier den Ordner angeben, der auf Änderungen überwacht werden soll
$monitoredName = '*'                                                            # Sollen nur auf Ordner mit bestimmtem Namen reagiert werden, hier angeben

if (-Not(Test-Path $monitoredDirectory)) {
    Write-Host "Kein gueltiger zu Ueberwachender Ordner angegeben, Skript wird abgebrochen" -Fore red
    exit
}

$fileSystemWatcher = New-Object IO.FileSystemWatcher $monitoredDirectory, $monitoredName -Property @{IncludeSubdirectories = $false; NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite', 'DirectoryName'}

Register-ObjectEvent $fileSystemWatcher Created -SourceIdentifier FileCreated -Action { # Ordnerueberwachung starten

    $name = $Event.SourceEventArgs.Name
    $time = $Event.TimeGenerated
    $fullPath = $Event.SourceEventArgs.FullPath                                 # Pfad des neu erstellten Ordners holen

    Write-Host "Event: Ordner $name erstellt um $time - Pfad: $fullPath" -fore green
}