# Copyright (c) 2018 Christopher Gundlach
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

<#
.SYNOPSIS
Dieses Skript wird einen angegebenen Ordner auf neu erstellte Ordner überwachen und dann in neu erstellten Ordnern die Ordnerstruktur eines Vorlagenordners duplizieren
.DESCRIPTION
Die Variable 'monitoredDirectory' muss mit dem Pfad gefuellt werden, der ueberwacht werden soll
Die Variable 'templatePath' muss mit dem Pfad gefuellt werden, dessen Struktur dupliziert werden soll.
Wichtig dabei: Kein schliessendes '\'

Bei neu erstellten Ordnern im zu ueberwachenden Ordner wird der Vorlagenordner rekursiv durchgegangen und die Ordnerstruktur genau so im neu erstellten Ordner erstellt.
Das Event feuert sowohl bei neu erstellten Ordnern, als auch bei kopierten oder verschobenen Ordnern
#>

$monitoredDirectory = 'D:\Test\Test-Ordner'                                # Hier den Ordner angeben, der auf Änderungen überwacht werden soll
$monitoredName = '*'                                                            # Sollen nur auf Ordner mit bestimmtem Namen reagiert werden, hier angeben

if (-Not(Test-Path $monitoredDirectory)) {
    Write-Host "Kein gueltiger zu Ueberwachender Ordner angegeben, Skript wird abgebrochen" -Fore red
    exit
}

$fileSystemWatcher = New-Object IO.FileSystemWatcher $monitoredDirectory, $monitoredName -Property @{IncludeSubdirectories = $false; NotifyFilter = [IO.NotifyFilters]'DirectoryName'}

Register-ObjectEvent $fileSystemWatcher Created -SourceIdentifier DirectoryCreated -Action { # Ordnerueberwachung starten

    function Copy-DirectoryStructure($Path, $Template) {
        # Get-ChildItem -Path $Template -Directory | ForEach-Object {             # Geht erst ab PowerShell 3.0
        Get-ChildItem -Path $Template | Where-Object { $_.PSIsContainer } | ForEach-Object {             # Alle Ordner im Vorlagenordner abholen und abarbeiten
            $newDirectory = $path + '\' + $_.name                               # Pfad des Zielverzeichnisses erstellen
            if (-Not (Test-Path -Path $newDirectory -PathType Container)) {     # Sicherstellen, dass das Zielverzeichnis noch nicht existiert
                New-Item -Path $newDirectory -ItemType Directory                # Verzeichnis erstellen
            }
            Copy-DirectoryStructure -Path $newDirectory -Template $_.FullName   # Mit dem nächsten Unterordner weitermachen. Rekursion!
        }
    }   

    $fullPath = $Event.SourceEventArgs.FullPath                                 # Pfad des neu erstellten Ordners holen

    $path = $fullPath -replace '\\(?:.(?!\\))+$', ''                            # Regex: entferne letzte Ordnerebene. E.g. "C:\Ordner\Neuer Ordner" wird zu "C:\Ordner"
    $templatePath = 'D:\Test\Test-Ordner\Vorlage'                          # Hier den Ordner angeben, dessen Struktur kopiert werden soll

    if (-Not(Test-Path $templatePath)) {
        Write-Host "Kein gueltiger Vorlagenordner angegeben, Ueberwachung wird abgebrochen" -Fore red
        Unregister-Event DirectoryCreated
        exit
    }
    
    Copy-DirectoryStructure -Path $FullPath -Template $templatePath            # Führt die oben definierte Funktion aus

}
# Um das Event nicht mehr überwachen zu lassen folgendes Commandlet in dem Powershell-Fesnter ausführen, mit dem dieses Skript ausgeführt wurde:
# Unregister-Event DirectoryCreated
# Alternativ kann das entsprechende Fenster auch geschlossen bzw. der Prozess beendet werden

# EventÜberwachung laufen lassen - Zum automatischen Start via Aufgabenplanung gedacht
while ($true) {
    start-sleep -milliseconds 500
}